package com.example.lab1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    /**
     * @param a - first value
     * @param b - second value
     * @return - max of a and b
     */
    public static int max(int a, int b) {
        return a > b ? a : b;
    }

    /**
     * @param a - first value
     * @param b - second value
     * @return - min of a and b
     */
    public static int min(int a, int b) {
        return a < b ? a : b;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}