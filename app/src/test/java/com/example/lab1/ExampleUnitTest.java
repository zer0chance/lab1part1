package com.example.lab1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void max_isCorrect() {
        assertEquals(2, MainActivity.max(1, 2));
    }

    @Test
    public void min_isCorrect() {
        assertEquals(1, MainActivity.min(1, 2));
    }

    @Test
    public void maxSame_isCorrect() {
        assertEquals(100, MainActivity.max(100, 100));
    }

    @Test
    public void minSame_isCorrect() {
        assertEquals(100, MainActivity.min(100, 100));
    }
}